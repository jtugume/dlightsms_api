<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Utils\Paginate;

class ApiDeliveryReportController extends Controller
{
    public function index() 
    {
        // get data
        // staff with r/s list
        // $list
        //$apideliveryreports = \App\Models\ApiBillLog::all();


        if (request()->start_date || request()->end_date) {

            $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            //$apideliveryreports = \App\Models\ApiBillLog::whereBetween('ts_stamp',[$start_date,$end_date])->get();
            $apideliveryreports = \App\Models\ApiBillLog::select(DB::raw("DATE_FORMAT(ts_stamp, '%Y-%m-%d') as day"), DB::raw("COUNT(*) as total_sms"),"dlrvalue","senderid")
            ->whereBetween('ts_stamp',[$start_date,$end_date])
            ->groupBy('day','dlrvalue','senderid')
            ->orderBy('day', 'desc')
            ->get(); 

            $apideliveryreports = $apideliveryreports->transform(function ($item, $key) {
                switch ($item->dlrvalue) {
                    case 1:
                        $item->dlrvalue = "DELIVERED";
                        break;
                    case 2:
                        $item->dlrvalue = "NOT-DELIVRD TO PHONE";
                        break;
                    case 4:
                        $item->dlrvalue = "QUEUED ON SMSC";
                        break;           
                    case 8:
                        $item->dlrvalue = "SENT TO SMSC";
                        break;
                    case 16:
                        $item->dlrvalue = "NON-DELIVRD TO SMSC";
                        break;
                    case 34:
                        $item->dlrvalue = "EXPIRED";
                        break;
                }
                return $item;
            })->toArray();

        } else {

            //$apideliveryreports = \App\Models\ApiBillLog::all();

            $apideliveryreports = \App\Models\ApiBillLog::select(DB::raw("DATE_FORMAT(ts_stamp, '%Y-%m-%d') as day"), DB::raw("COUNT(*) as total_sms"),"dlrvalue","senderid")
            ->groupBy('day','dlrvalue','senderid')
            ->orderBy('day', 'desc')
            ->get();  

            $apideliveryreports = $apideliveryreports->transform(function ($item, $key) {
                switch ($item->dlrvalue) {
                    case 1:
                        $item->dlrvalue = "DELIVERED";
                        break;
                    case 2:
                        $item->dlrvalue = "NOT-DELIVRD TO PHONE";
                        break;
                    case 4:
                        $item->dlrvalue = "QUEUED ON SMSC";
                        break;           
                    case 8:
                        $item->dlrvalue = "SENT TO SMSC";
                        break;
                    case 16:
                        $item->dlrvalue = "NON-DELIVRD TO SMSC";
                        break;
                    case 34:
                        $item->dlrvalue = "EXPIRED";
                        break;
                }
                return $item;
            })->toArray();
            //dd($data->toArray());

            //dd($data);

        }
        $apideliveryreports = Paginate::paginate($apideliveryreports,5)->setPath(route('reports.apideliveryreports'))->appends(['start_date' => request()->start_date, 'end_date' => request()->end_date]);
        //dd($apideliveryreports->toArray());
        return view('reports.apideliveryreports', compact('apideliveryreports'));
        //$list->name
    }

    public function numbers(Request $request)
    {
        if ($request->has('search')) {
            
            $numbers = \App\Models\ApiBillLog::select(DB::raw("DATE_FORMAT(ts_stamp, '%Y-%m-%d') as day"),"phonenum","dlrvalue","senderid")
            ->where('phonenum', 'like', '%' . request('search') . '%')   
            ->orderBy('day', 'desc')->get();

            //dd($numbers);

            $numbers = $numbers->transform(function ($item, $key) {
                switch ($item->dlrvalue) {
                    case 1:
                        $item->dlrvalue = "DELIVERED";
                        break;
                    case 2:
                        $item->dlrvalue = "NOT-DELIVRD TO PHONE";
                        break;
                    case 4:
                        $item->dlrvalue = "QUEUED ON SMSC";
                        break;           
                    case 8:
                        $item->dlrvalue = "SENT TO SMSC";
                        break;
                    case 16:
                        $item->dlrvalue = "NON-DELIVRD TO SMSC";
                        break;
                    case 34:
                        $item->dlrvalue = "EXPIRED";
                        break;
                }
                return $item;
            })->toArray();


        } else {

            $numbers = \App\Models\ApiBillLog::select(DB::raw("DATE_FORMAT(ts_stamp, '%Y-%m-%d') as day"),"phonenum","dlrvalue","senderid")
            ->orderBy('day', 'desc')
            ->get(); 

            $numbers = $numbers->transform(function ($item, $key) {
                switch ($item->dlrvalue) {
                    case 1:
                        $item->dlrvalue = "DELIVERED";
                        break;
                    case 2:
                        $item->dlrvalue = "NOT-DELIVRD TO PHONE";
                        break;
                    case 4:
                        $item->dlrvalue = "QUEUED ON SMSC";
                        break;           
                    case 8:
                        $item->dlrvalue = "SENT TO SMSC";
                        break;
                    case 16:
                        $item->dlrvalue = "NON-DELIVRD TO SMSC";
                        break;
                    case 34:
                        $item->dlrvalue = "EXPIRED";
                        break;
                }
                return $item;
            })->toArray();

        }

        $numbers = Paginate::paginate($numbers,5)->setPath(route('reports.apideliveryreportsdetails'))->appends(['start_date' => request()->start_date, 'end_date' => request()->end_date]);
        
        return view('reports.apideliveryreportsnumbers', compact('numbers'));
        
    }

    public function autoComplete(Request $request) {

        $query = $request->get('term');
        
        $products= \App\Models\ApiBillLog::where('phonenum','LIKE','%'.$query.'%')->get();
        
        return response()->json($products);
        
    }

    public function details(Request $request)
    {

        if (request()->start_date || request()->end_date) {

            $start_date = Carbon::parse(request()->start_date)->toDateTimeString();
            $end_date = Carbon::parse(request()->end_date)->toDateTimeString();
            //$apideliveryreports = \App\Models\ApiBillLog::whereBetween('ts_stamp',[$start_date,$end_date])->get();
            $apideliveryreports = \App\Models\ApiBillLog::select(DB::raw("DATE_FORMAT(ts_stamp, '%Y-%m-%d') as day"),"phonenum","dlrvalue","senderid")
            ->whereBetween('ts_stamp',[$start_date,$end_date])
            ->orderBy('day', 'desc')
            ->get(); 

            $apideliveryreports = $apideliveryreports->transform(function ($item, $key) {
                switch ($item->dlrvalue) {
                    case 1:
                        $item->dlrvalue = "DELIVERED";
                        break;
                    case 2:
                        $item->dlrvalue = "NOT-DELIVRD TO PHONE";
                        break;
                    case 4:
                        $item->dlrvalue = "QUEUED ON SMSC";
                        break;           
                    case 8:
                        $item->dlrvalue = "SENT TO SMSC";
                        break;
                    case 16:
                        $item->dlrvalue = "NON-DELIVRD TO SMSC";
                        break;
                    case 34:
                        $item->dlrvalue = "EXPIRED";
                        break;
                }
                return $item;
            })->toArray();

        } else {

            $apideliveryreports = \App\Models\ApiBillLog::select(DB::raw("DATE_FORMAT(ts_stamp, '%Y-%m-%d') as day"),"phonenum","dlrvalue","senderid")
            ->orderBy('day', 'desc')
            ->get();  

            $apideliveryreports = $apideliveryreports->transform(function ($item, $key) {
                switch ($item->dlrvalue) {
                    case 1:
                        $item->dlrvalue = "DELIVERED";
                        break;
                    case 2:
                        $item->dlrvalue = "NOT-DELIVRD TO PHONE";
                        break;
                    case 4:
                        $item->dlrvalue = "QUEUED ON SMSC";
                        break;           
                    case 8:
                        $item->dlrvalue = "SENT TO SMSC";
                        break;
                    case 16:
                        $item->dlrvalue = "NON-DELIVRD TO SMSC";
                        break;
                    case 34:
                        $item->dlrvalue = "EXPIRED";
                        break;
                }
                return $item;
            })->toArray();
            //dd($data->toArray());

            //dd($data);

        }
        $apideliveryreports = Paginate::paginate($apideliveryreports,5)->setPath(route('reports.apideliveryreportsdetails'))->appends(['start_date' => request()->start_date, 'end_date' => request()->end_date]);
        
        return view('reports.apideliveryreportsdetails', compact('apideliveryreports'));
       

    }

    public function exportCSV(Request $request)
    {
        if ($request->start_date || $request->end_date){

            $start_date = Carbon::parse($request->start_date)->toDateTimeString();
            $end_date = Carbon::parse($request->end_date)->toDateTimeString();

            $fileName = 'api_delivery_report.csv';
            $tasks = \App\Models\ApiBillLog::select(DB::raw("DATE_FORMAT(ts_stamp, '%Y-%m-%d') as day"), DB::raw("COUNT(*) as total_sms"),"dlrvalue","senderid")
            ->whereBetween('ts_stamp',[$start_date,$end_date])
            ->groupBy('day','dlrvalue','senderid')
            ->orderBy('day', 'desc')
            ->get();  

            $tasks = $tasks->transform(function ($item, $key) {

                switch ($item->dlrvalue) {

                    case 1:
                        $item->dlrvalue = "DELIVRD";
                        break;
                    case 2:
                        $item->dlrvalue = "NOT-DELIVRD TO PHONE";
                        break;
                    case 4:
                        $item->dlrvalue = "QUEUED ON SMSC";
                        break;           
                    case 8:
                        $item->dlrvalue = "SENT TO SMSC";
                        break;
                    case 16:
                        $item->dlrvalue = "NON-DELIVRD TO SMSC";
                        break;
                    case 34:
                        $item->dlrvalue = "EXPIRED";
                        break;
                }
                return $item;

            })->toArray();

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Sender ID', 'DateTime', 'Status', 'Total SMS');

            $callback = function() use($tasks, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($tasks as $task) {

                    $row['Sender ID'] = $task['senderid'];
                    $row['DateTime']  = $task['day'];
                    $row['Status']    = $task['dlrvalue'];
                    $row['Total SMS'] = $task['total_sms'];
                    

                    fputcsv($file, array($row['Sender ID'], $row['DateTime'], $row['Status'], $row['Total SMS']));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);

        }else{

            $fileName = 'api_delivery_report.csv';
            $tasks = \App\Models\ApiBillLog::select(DB::raw("DATE_FORMAT(ts_stamp, '%Y-%m-%d') as day"), DB::raw("COUNT(*) as total_sms"),"dlrvalue","senderid")
            ->groupBy('day','dlrvalue','senderid')
            ->orderBy('day', 'desc')
            ->get();  

            $tasks = $tasks->transform(function ($item, $key) {

                switch ($item->dlrvalue) {

                    case 1:
                        $item->dlrvalue = "DELIVRD";
                        break;
                    case 2:
                        $item->dlrvalue = "NOT-DELIVRD TO PHONE";
                        break;
                    case 4:
                        $item->dlrvalue = "QUEUED ON SMSC";
                        break;           
                    case 8:
                        $item->dlrvalue = "SENT TO SMSC";
                        break;
                    case 16:
                        $item->dlrvalue = "NON-DELIVRD TO SMSC";
                        break;
                    case 34:
                        $item->dlrvalue = "EXPIRED";
                        break;
                }
                return $item;

            })->toArray();

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Sender ID', 'DateTime', 'Status', 'Total SMS');

            $callback = function() use($tasks, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($tasks as $task) {

                    $row['Sender ID'] = $task['senderid'];
                    $row['DateTime']  = $task['day'];
                    $row['Status']    = $task['dlrvalue'];
                    $row['Total SMS'] = $task['total_sms'];
                    

                    fputcsv($file, array($row['Sender ID'], $row['DateTime'], $row['Status'], $row['Total SMS']));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        }
        
    }

    public function exportCSV2(Request $request)
    {

        if ($request->start_date || $request->end_date){

            $start_date = Carbon::parse($request->start_date)->toDateTimeString();
            $end_date = Carbon::parse($request->end_date)->toDateTimeString();

            $fileName = 'api_delivery_report_details.csv';
            $tasks = \App\Models\ApiBillLog::select(DB::raw("DATE_FORMAT(ts_stamp, '%Y-%m-%d') as day"), "phonenum","dlrvalue","senderid")
            ->whereBetween('ts_stamp',[$start_date,$end_date])
            ->orderBy('day', 'desc')
            ->get();  

            $tasks = $tasks->transform(function ($item, $key) {

                switch ($item->dlrvalue) {

                    case 1:
                        $item->dlrvalue = "DELIVRD";
                        break;
                    case 2:
                        $item->dlrvalue = "NOT-DELIVRD TO PHONE";
                        break;
                    case 4:
                        $item->dlrvalue = "QUEUED ON SMSC";
                        break;           
                    case 8:
                        $item->dlrvalue = "SENT TO SMSC";
                        break;
                    case 16:
                        $item->dlrvalue = "NON-DELIVRD TO SMSC";
                        break;
                    case 34:
                        $item->dlrvalue = "EXPIRED";
                        break;
                }
                return $item;

            })->toArray();

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Sender ID', 'DateTime','Phone Number','Status');

            $callback = function() use($tasks, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($tasks as $task) {

                    $row['Sender ID'] = $task['senderid'];
                    $row['DateTime']  = $task['day'];
                    $row['Phone Number']  = $task['phonenum'];
                    $row['Status']    = $task['dlrvalue'];
                    
                    fputcsv($file, array($row['Sender ID'], $row['DateTime'], $row['Phone Number'], $row['Status']));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);

        }else{

            $fileName = 'api_delivery_report_details.csv';
            $tasks = \App\Models\ApiBillLog::select(DB::raw("DATE_FORMAT(ts_stamp, '%Y-%m-%d') as day"), "phonenum","dlrvalue","senderid")
            ->orderBy('day', 'desc')
            ->get();  

            $tasks = $tasks->transform(function ($item, $key) {

                switch ($item->dlrvalue) {

                    case 1:
                        $item->dlrvalue = "DELIVRD";
                        break;
                    case 2:
                        $item->dlrvalue = "NOT-DELIVRD TO PHONE";
                        break;
                    case 4:
                        $item->dlrvalue = "QUEUED ON SMSC";
                        break;           
                    case 8:
                        $item->dlrvalue = "SENT TO SMSC";
                        break;
                    case 16:
                        $item->dlrvalue = "NON-DELIVRD TO SMSC";
                        break;
                    case 34:
                        $item->dlrvalue = "EXPIRED";
                        break;
                }
                return $item;

            })->toArray();

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Sender ID', 'DateTime','Phone Number','Status');

            $callback = function() use($tasks, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($tasks as $task) {

                    $row['Sender ID'] = $task['senderid'];
                    $row['DateTime']  = $task['day'];
                    $row['Phone Number']  = $task['phonenum'];
                    $row['Status']    = $task['dlrvalue'];
                    
                    fputcsv($file, array($row['Sender ID'], $row['DateTime'], $row['Phone Number'], $row['Status']));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);


        }
        
    }

}
