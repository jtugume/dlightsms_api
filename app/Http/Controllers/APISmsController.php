<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;
use App\Models\User;

use App\Models\SmsCredit;
use Illuminate\Support\Facades\Log;

class APISmsController extends Controller
{
    //
    public function sms_api(Request $request){

        $sender   = $request->to;
        $dest     = $request->from;
        $content  = $request->text;
        $username = $request->username;
        $password = $request->password;
        
        $ip_address = $_SERVER['REMOTE_ADDR'];

        if (!isset($username) || (strlen($username) < 5)) {
            return json_encode(array("code" => "401", "message" => "Missing Username"));
            
        }
    
   
        if ((strlen($password) < 5) || !isset($password)) {
            return json_encode(array("code" => "402", "message" => "Missing or invalid password"));
                
        }
    
        //authenticate the user first
        if (!$this->user_auth($username,$password)) {
            return json_encode(array("code" => "403", "message" => "Invalid username or password"));
            
        }
    
        if (empty($sender) || !isset($sender)) {
            return json_encode(array("code" => "404", "message" => "Missing Phone number"));
           
        }
    
        if (empty($dest) || !isset($dest)) {
            return json_encode(array("code" => "405", "message" => "Missing Sender ID"));
            
        }
    
        if ((strlen($content) < 3) || !isset($content)) {
            return json_encode(array("code" => "406", "message" => "Missing Content"));
           
        }
    
    
        $user_details = $this->get_user_details($username);
        
        try{

            for ($i=0; $i<=count($user_details); $i++) {
                    
                    $staff_id = $user_details[$i]->id;
                    //dd( $staff_id);
                    $ran_number = mt_rand(1000,1000000);
                    
                    // convert sender into array to cater for numbers
                    $array_nums = $this->convert_to_array($sender);
                    $content = addslashes($content);
                    
                    $count_num = sizeof($array_nums);
                    
                    $this->bill_logs($dest,$count_num,$content,$username,$ip_address);
                    
                    // deduct the credits
                    $no_of_msgs = $this->getCount($request->text);
            
                    $total_count = $count_num * $no_of_msgs;
                    /*
                    if (!$this->check2credit($staff_id,$total_count) ) {
            
                        $ans =  "You don't have enough credit.  Please try again";

                        $arr_sms = array("code" => "404", "remarks" => $ans,"status" => "NO CREDITS");

                        return json_encode($arr_sms);                     
                    
                    }
                    */
                   $this->deduct2credit($staff_id,$total_count);
            
                    foreach($array_nums as $key => $value) {
                        // lets send the SMS
                      
                        $msgid = rand(000001,999999);
                        // if invalid phone number, then skip it.256712000000
                        if(preg_match('/^[0-9]{12}$/', $value) > 0){
                    
                            if (!$this->isbanned($content)) {      // send2sms_route($dest,$sender,$msg,$userid)
                                // print $route."\n";
                                $list_id = 1;
                                $msg = $content;
                                
                                $sent = $this->send2sms_route($dest,$sender,$msg,$staff_id,$msgid);

                                $this->log2sms($sender,$dest,$content,$list_id,$staff_id);
            
                                if ($sent) {
            
                                    $arr_sms = array("code" => "200", "sender" => $value,"message_id" => $msgid,"remarks" => "Message Submitted Successfully","status" => "Sent");
                                    return json_encode($arr_sms);
                                   
                                }
            
                                $this->log_insms($username,$value,$content,$dest,$ip_address,$msgid);
            
                            } else {
            
                                $arr_sms = array("code" => "400", "sender" => $value,"message_id" => $msgid,"remarks" => "Message Failure","status" => "FAILED");
                                return json_encode($arr_sms);
                                
                            }
            
                        } else {
            
                            Log::info("Invalid phone number discarded");
            
                        }
                    
                        // lets log the SMS
                        $list_id = 2;   // since it's a single sms we assume it's the first one.
                        $rand_job_id = date('is');
                        $job_id = $list_id.$list_id.$rand_job_id;
                        DB::table('bill_logs')->insert([                  
                            'list_id' => $list_id,
                            'user_id' => $staff_id,
                            'sender_id' => $dest,
                            'msg' => $content,
                            'job_id' => $job_id,
                            'units' => $total_count,
                            'ts_stamp' => \Carbon\Carbon::now()->toDateTimeString()
                        ]);

                    }

            }

        }catch(\Exception $e){

            Log::error($e->getMessage());
        }

    }

    // get the count of messages.
    public function getCount($content) {
        $len = strlen($content);
        $num = ceil($len / 159);
        return $num;
    }

    // public function check2credit($staff_id,$total_count) {
    
    //     $sms_credit = SmsCredit::whereUser_id($staff_id)->first();

    //     if ($sms_credit) {
    //         return $sms_credit->units>$total_count;
    //     }
    //     return false;
      
    // }
    //helper methods
    public function log_insms($username,$phone,$content,$dest,$ip_address,$msgid) {
        
        DB::table('api_log')->insert([
            'username' => $username,
            'sender' => $phone,
            'dest' => $dest,
            'message' => $content,
            'ip_address' => $ip_address,
            'msgid' => $msgid,
            'ts_stamp' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        Log::info("api sms logged successfully");

    }

    public function isbanned($message){

        $message = strtolower($message);

         if (stristr($message,"fuck") ||  stristr($message,"museveni") || stristr($message,"president")
                 || stristr($message,"government") || stristr($message,"upc") || stristr($message,"nrm")
                 || stristr($message,"kill") || stristr($message,"fdc") || stristr($message,"army")
                 || stristr($message,"politic") || stristr($message,"bitch") || stristr($message,"murder")){
                    return true;
        }else{
                return false;
        }

    }

    // log all sent SMS very important...
    public function log2sms($msisdn,$senderid,$msg,$list_id,$staff_id) {
        $id = $staff_id; 
        $rand_job_id = date('is');
        $job_id = $id.$list_id.$rand_job_id;

        DB::table('sent_sms')->insert([
            'sender' => $msisdn,
            'dest' => $senderid,
            'message' => $msg,
            'list_id' =>$list_id,
            'user_id' =>$staff_id,
            'job_id' => $job_id  
        ]);
        Log::info("sent sms logged successfully");
       
    }

    public function bill_logs($dest,$count,$content,$username,$ip_address) {
       
        DB::table('api_bill_logs')->insert([
            'dest' => $dest,
            'count' => $count,
            'content' => $content,
            'username' => $username,
            'ip_address' => $ip_address
        ]);

        Log::info("api bill logs inserted successfully");
    }

    // if user is authenticated, let's get their details
    public function get_user_details($username) {

        $sql =DB::select("SELECT * FROM users WHERE username ='$username'");
        //dd($sql);
        return $sql;
    }

    public function convert_to_array($nums) {
        $num_arr = explode(",",$nums);
        return $num_arr;
    }

    // authenticate user logins
    function user_auth($username, $pass) {

         $password =$pass;
        
         $user  = User::whereUsername($username)->first();
          
        if ( Hash::check("$pass",$user->password??null)) {
            return true;
        } else {
            return false;
        }

    }

    public function deduct2credit($staff_id,$total_count) {
        // if all is ok. Now you can deduct the credit
        DB::update('UPDATE sms_credits SET units = units - '.$total_count.' WHERE user_id  = ?',[$staff_id]);
    
        Log::info("Successfully deducted credits");
    }

    public function randomAlphaNum($length){

        $rangeMin = pow(36, $length-1); //smallest number to give length digits in base 36
        $rangeMax = pow(36, $length)-1; //largest number to give length digits in base 36
        $base10Rand = mt_rand($rangeMin, $rangeMax); //get the random number
        $newRand = base_convert($base10Rand, 10, 36); //convert it
    
        return $newRand; //spit it out
    
    }

    public function send2sms_route($dest,$sender,$msg,$staff_id,$msgid) {

        // what if you have more than one link we can send out using separate connections.
        $sender = trim($sender);
        $sender = intval($sender);
        
        $str = substr($sender,0,-7);
        $msg = urlencode($msg);
    
        $dest = urlencode($dest);
    
        $dlrmask = 31;
        $dest = "8177";
    
        $ran_number = $this->randomAlphaNum(12);
         
        $dlrurl = "https://api-dlightsms.qed.co.ug/api_dlr?";

        $q_str = "log_no=$ran_number&message=$msg&user_id=$staff_id&senderid=$dest&phonenum=%p&smscid=%i&ts_stamp=%t&dlrvalue=%d&smsid=%I";
       
        $str_url = urlencode($q_str);
        
        $dlrurl = $dlrurl.$str_url;
    
    
        switch($str) {
            case '25678':
                $dest ='8177';
                $cmd = 'http://localhost:13013/cgi-bin/sendsms?coding=0&smsc=SMSC02&to='.$sender.'&from='.$dest.'&text='.$msg.'&username=smsadmin&password=smsadmin123';
                break;
            case '25677':
                $dest ='8177';
                $cmd = 'http://localhost:13013/cgi-bin/sendsms?coding=0&smsc=SMSC02&to='.$sender.'&from='.$dest.'&text='.$msg.'&username=smsadmin&password=smsadmin123';
                break;
            case '25676':
                $dest ='8177';
                $cmd = 'http://localhost:13013/cgi-bin/sendsms?coding=0&smsc=SMSC02&to='.$sender.'&from='.$dest.'&text='.$msg.'&username=smsadmin&password=smsadmin123';
                break;
             case '25675':
                $dest = '8177';
                $cmd = 'http://localhost:13013/cgi-bin/sendsms?coding=0&smsc=SMSC03&to='.$sender.'&from='.$dest.'&text='.$msg.'&username=smsadmin&password=smsadmin123';
                break;
             case '25674':
                $dest = '8177';
                $cmd = 'http://localhost:13013/cgi-bin/sendsms?coding=0&smsc=SMSC03&to='.$sender.'&from='.$dest.'&text='.$msg.'&username=smsadmin&password=smsadmin123';
                break;
             case '25670':
                $dest = '8177';    
                $cmd = 'http://localhost:13013/cgi-bin/sendsms?coding=0&smsc=SMSC03&to='.$sender.'&from='.$dest.'&text='.$msg.'&username=smsadmin&password=smsadmin123';
                break; 
            default:
                $dest ='8177';
                $cmd = 'http://localhost:13013/cgi-bin/sendsms?coding=0&smsc=SMSC02&to='.$sender.'&from='.$dest.'&text='.$msg.'&username=smsadmin&password=smsadmin123';
        }		
    
        $cmd = $cmd.'&dlr-mask=31&dlr-url='.$dlrurl;

        Log::info($cmd);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$cmd);
        curl_exec($ch);
        curl_close($ch);

        return 1;
    }

}
