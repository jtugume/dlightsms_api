<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Auth\DashboardController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PermissionsController;
use App\Http\Controllers\ScheduleMessageController;
use App\Http\Controllers\AssignCreditController;
use App\Http\Controllers\ApiDeliveryReportController;
use App\Http\Controllers\APISmsController;
use App\Http\Controllers\AuditlogsController;
use App\Http\Controllers\MoSmsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/smsapi', [APISmsController::class,'sms_api']);

Route::get('/usercharts',[DashboardController::class,'user_charts']);
Route::get('/apitokensmtn',[DashboardController::class,'tokens_mtn']);
Route::get('/apitokensairtel',[DashboardController::class,'tokens_airtel']);

Route::get('/dlr',[DashboardController::class,'bulksmsdeliveryreports']);
    
Route::get('/api_dlr',[DashboardController::class,'apideliveryreports']);

Route::middleware('auth')->group(function() {

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
Route::get('logout', [AuthController::class, 'logout'])->name('logout'); 
});

Route::middleware('loggedin')->group(function() {
    Route::get('login', [AuthController::class, 'loginView'])->name('login-view');
    Route::post('login', [AuthController::class, 'login'])->name('login');
    
    
    //Route::get('register', [AuthController::class, 'registerView'])->name('register-view');
    //Route::post('register', [AuthController::class, 'register'])->name('register');
});

Route::resource('roles', RoleController::class);

Route::resource('permissions', PermissionsController::class);
// Route::resource('roles', RoleController::class);
// Route::resource('permissions', PermissionsController::class);

/**
 * User Routes
 */
Route::group(['prefix' => 'users'], function() {
    Route::get('/', [UserController::class ,'index'])->name('users.index');
    Route::get('/create', [UserController::class ,'create'])->name('users.create');
    Route::post('/create', [UserController::class ,'store'])->name('users.store');
    Route::get('/{user}/show', [UserController::class ,'show'])->name('users.show');
    Route::get('/{user}/edit', [UserController::class ,'edit'])->name('users.edit');
    Route::post('/{user}/update', [UserController::class ,'update'])->name('users.update');
    Route::get('/{user}/delete', [UserController::class ,'destroy'])->name('users.destroy');
    Route::get('/units', [UserController::class ,'units'])->name('users.units');
    
});

Route::get('/users', [UserController::class ,'index'])->name('users.index')->middleware('auth');

Route::get('register', [AuthController::class, 'registerView'])->name('register-view')->middleware('auth');

Route::post('register', [AuthController::class, 'register'])->name('register')->middleware('auth');

Route::get('/audittrail', [AuditlogsController::class,'index'])->name('reports.audittrail')->middleware('auth');

Route::get('/apideliveryreports', [ApiDeliveryReportController::class,'index'])->name('reports.apideliveryreports')->middleware('auth');

Route::get('/apideliveryreportsdetailed', [ApiDeliveryReportController::class,'details'])->name('reports.apideliveryreportsdetails')->middleware('auth');

Route::post('/apideliveryreportsnumbers', [ApiDeliveryReportController::class,'numbers'])->name('reports.apideliveryreportsnumbers')->middleware('auth');

Route::get('/apideliveryreportsautocomplete', [ApiDeliveryReportController::class,'autoComplete'])->name('search.autocomplete')->middleware('auth');

Route::get('/schedulesms', [ScheduleMessageController::class,'index'])->name('broadcasts.schedulemessages')->middleware('auth');

Route::get('/assigncredit', [AssignCreditController::class,'index'])->name('credits.assigncredit')->middleware('auth');

Route::get('/export-csv2',[ApiDeliveryReportController::class,'exportCSV'])->middleware('auth')->name('reports.apideliveryreportexport')->middleware('auth');

Route::get('/export-csv4',[ApiDeliveryReportController::class,'exportCSV2'])->name('reports.apideliveryreportexportdetailed')->middleware('auth');


